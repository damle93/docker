FROM debian:wheezy
MAINTAINER Linh P <linhpth@gmail.com>

RUN apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0x5a16e7281be7a449; \
	echo deb http://dl.hhvm.com/debian wheezy main | tee /etc/apt/sources.list.d/hhvm.list; \
	apt-get update; \
	apt-get -y install --no-install-recommends mysql-client hhvm  nginx curl unzip ca-certificates; \
	/usr/share/hhvm/install_fastcgi.sh; \
	echo "\ndaemon off;" >> /etc/nginx/nginx.conf

# Install vi
RUN apt-get -y install vim

# Install composer
RUN cd /usr/local/sbin/ && curl -sS -k https://getcomposer.org/installer | php && mv composer.phar composer

# No need unzip, curl anymore
RUN apt-get -y remove unzip curl

# Install Laravel
RUN mkdir /webapp
COPY src/ /webapp/
RUN cd /webapp && composer install

WORKDIR /webapp

EXPOSE 80

ADD laravel/php.ini /etc/hhvm/php.ini
ADD laravel/start.sh /start.sh
ADD laravel/nginx-default /etc/nginx/sites-enabled/default

CMD ["/bin/bash", "/start.sh"]