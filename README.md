# WPL Platform

## Requirements

* Linux system or vm (you can use vagrant with virtualbox)
* [Docker](https://www.docker.com)
* [Docker compose](https://docs.docker.com/compose/)


## Virtualbox instructions

You can run linux virtual maching using virtualbox.

* Install [virtualbox](https://www.virtualbox.org/wiki/Downloads)
* Install [vagrant](https://www.vagrantup.com/downloads.html)

Open terminal or "Git bash" (Windows) and clone vagrant script to same folder as wpl-platform-server (next to it).

```
git clone git@bitbucket.org:devops_epsilon/vagrant-docker.git
```

## Start virtual machine

```
cd vagrant-docker
vagrant up

vagrant ssh
docker -v
docker-compose -v
```

## Usage

* Change to project directory (`cd /share/wpl-platform-server` with vagrant)
* First time run `./build.sh`
* Daily use in development mode `docker-compose up`
* Open browser to localhost:1421 or 192.168.14.22:1421 with vagrant
* Login: admin/##wplplatform##

## Web server

* Nginx
* PHP Laravel
* Mysql
