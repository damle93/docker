#!/bin/sh
set -e

REPOS="docker.epsilon-mobile.com"
PROJECT="amp_platform"
VERSION="latest"

docker build -t $REPOS/$PROJECT .