#!/bin/sh
set -e

CURDATE=$(date +'%m-%d-%Y')
BACKUP_DIR='/srv/docker/amp-platform/mysql/backups'
DB_CONTAINER='ampplatformserver_mysql_1'

mkdir -p $BACKUP_DIR

docker exec -i $DB_CONTAINER sh -c "mysqldump -uroot -proot amp_platform > /var/lib/mysql/backups/amp_platform_$CURDATE.backup.sql" > /dev/null