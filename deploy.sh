#!/bin/sh
set -e

WEB_CONTAINER='ampplatformserver_web_1'

echo "*** Deploying Production ***"

echo "Enable maintenance mode"
docker exec -i $WEB_CONTAINER php artisan down

echo "Backup DB"
./dbbackup.sh

echo "Remove web container"
docker-compose stop web
docker-compose rm -f web

echo "Start latest web container"
docker-compose up -d

echo "Wait for web container fully started"
sleep 10s

echo "Disable maintenance mode"
docker exec -i $WEB_CONTAINER php artisan up

echo "*** Finish ***"