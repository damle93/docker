#!/bin/bash

# To ensure that mysql is ready for connect
sleep 10s

MYSQL_DB="amp_platform"
DBCONFIG_FILE="app/config/database.php"

DBEXISTS=$(mysql -hmysql -P"$MYSQL_PORT_3306_TCP_PORT" -uroot -p"$MYSQL_ENV_MYSQL_ROOT_PASSWORD" --batch --skip-column-names -e "SHOW DATABASES LIKE '"$MYSQL_DB"';" | grep -q "$MYSQL_DB"; echo "$?")
# Create db if any
if [ ! $DBEXISTS -eq 0 ];then
	mysql -hmysql -P"$MYSQL_PORT_3306_TCP_PORT" -uroot -p"$MYSQL_ENV_MYSQL_ROOT_PASSWORD" -e "CREATE DATABASE "$MYSQL_DB";"
fi

# Update db config
sed -i "s/'localhost'/'mysql'/g" $DBCONFIG_FILE
sed -i "s/=> 'database'/=> '$MYSQL_DB'/g" $DBCONFIG_FILE

# Run composer install if vendor directory isn't exists
if [ ! -d vendor ]; then
	composer install
fi

php artisan --force migrate

/etc/init.d/hhvm start
/usr/sbin/nginx