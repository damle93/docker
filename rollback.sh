#!/bin/sh
set -e

REPOS="docker.epsilon-mobile.com"
PROJECT="amp-platform"
CURDATE=$(date +'%m-%d-%Y')
DB_CONTAINER='ampplatformserver_mysql_1'
WEB_CONTAINER='ampplatformserver_web_1'

echo "Rollback image"
docker-compose stop web
docker-compose rm -f web
docker rmi $REPOS/$PROJECT:latest
docker tag $REPOS/$PROJECT:previous $REPOS/$PROJECT:latest

echo "Rollback DB"
docker exec -i $DB_CONTAINER sh -c "mysqldump -uroot -proot amp_platform > /var/lib/mysql/backups/amp_platform_$CURDATE.failed.backup.sql" > /dev/null
docker exec -i $DB_CONTAINER sh -c "mysql -proot -e 'DROP DATABASE amp_platform'"
docker exec -i $DB_CONTAINER sh -c "mysql -proot -e 'CREATE DATABASE amp_platform'"
docker exec -i $DB_CONTAINER sh -c "mysql -proot amp_platform < /var/lib/mysql/backups/amp_platform_$CURDATE.backup.sql" > /dev/null

echo "Start web container"
docker-compose up -d

echo "Wait for web container fully started"
sleep 10s

echo "Disable maintenance mode"
docker exec -i $WEB_CONTAINER php artisan up

echo "Finished"